import pokeball from '../../../assets/images/pokeball.png';
import classes from './Loader.module.css';

const Loader = (props) => {
    return <img src={pokeball} alt="Loading..." className={`${classes.loader} ${props.className}`} />;
};

export default Loader;
